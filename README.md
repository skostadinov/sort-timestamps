# Sort array of JS Objects by Time Stamp

This module takes an array of JS Objects and sorts it by the "timestamp" criteria.

## Install

The installation is pretty straight forward
### npm

    npm i sort-timestamps

### yarn

    yarn add sort-timestamps

## Usage
### Ascending

    var sortTimestamps = require("sort-timestamps");
